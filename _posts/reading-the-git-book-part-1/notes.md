
## alias 
git config --global alias.graph 'log --oneline --graph --all'


## git diff
git diff compares the staging area with the working directory. It shows what you _could_ add to the staging area 

`git diff --staged` or `git diff --cached` shows the _staged_ changes compared to a commit (when not specified, defaults to `HEAD`)

`git checkout` and `git reset` do similar things but they are different in subtle ways:
it depends whether we call it with a path or not 



## branches
`git branch foo` only creates a branch 
git checkout -b and git switch -c both create branch and check it out
we can compare two branches with git diff foo main 

show commits not _reachable_ from the current branch 

![](commit-ranges.png)
